<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Users

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

Route::get('api/users', 'UserController@index');
Route::get('api/users/{id}', 'UserController@getUser');
Route::post('api/users', 'UserController@createUser');
Route::put('api/users/{id}', 'UserController@updateUser');
Route::delete('api/users/{id}', 'UserController@deleteUser');
