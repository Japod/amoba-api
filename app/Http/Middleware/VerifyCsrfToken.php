<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{

    protected $addHttpCookie = false;

    protected $except = [
        'http://127.0.0.1:8000/api/users',
        'http://localhost:8080/',
        'http://localhost:8080/api/users',
        'http://localhost:8080/api/users/{id}',
    ];
}
