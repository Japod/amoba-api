<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->paginate(10);
        // $phone = User::find(1)->profile;

        return response($users, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUser(Request $request)
    {

        $request->validate(
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'description' => 'required',
            ],
            [
                'first_name.required' => 'Name is required',
                'last_name.required' => 'Last name is required',
                'description.required' => 'Description is Required'
            ]
        );

        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->description = $request->description;

        $user->save();

        return response()->json([
            "message" => "Usuario creado con exito"
        ], 200);
    }

    public function updateUser(Request $request, $id)
    {

        $request->validate(
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'description' => 'required',
            ],
            [
                'first_name.required' => 'Name is required',
                'last_name.required' => 'Last name is required',
                'description.required' => 'Description is Required'
            ]
        );

        if (user::where('id', $id)->exists()) {
            $User = User::find($id);

            $User->first_name = is_null($request->first_name) ? $User->first_name : $request->first_name;
            $User->last_name = is_null($request->last_name) ? $User->last_name : $request->last_name;
            $User->description = is_null($request->description) ? $User->description : $request->description;
            $User->save();

            return response()->json([
                "message" => "User updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "User not found"
            ], 404);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function deleteUser($id)
    {
        if (User::where('id', $id)->exists()) {
            $user = User::find($id);
            $user->delete();

            return response()->json([
                "message" => "records deleted"
            ], 202);
        } else {
            return response()->json([
                "message" => "User not found"
            ], 404);
        }
    }
}
